#define _CRT_SECURE_NO_WARNINGS

#include "base/Defs.hpp"
#include "base/Math.hpp"
#include "RayTracer.hpp"
#include <stdio.h>
#include "rtIntersect.inl"
#include <fstream>
#include <algorithm>
#include <functional>

#include "rtlib.hpp"


// Helper function for hashing scene data for caching BVHs
extern "C" void MD5Buffer( void* buffer, size_t bufLen, unsigned int* pDigest );


namespace FW
{


Vec2f getTexelCoords(Vec2f uv, const Vec2i size)
{

	// YOUR CODE HERE (R3):
	// Get texel indices of texel nearest to the uv vector. Used in texturing.
	// uv should be mapped from [0,1]x[0,1] to the size of the image [0,X]x[0,Y] and
	// wrapped so that uv values beyond 1 are mapped back to [0,1] in a repeating manner
	// (so 0.5, 1.5, 2.5,... all map to 0.5)
	return Vec2i();
}

Mat3f formBasis(const Vec3f& n) {
    // YOUR CODE HERE (R4):
    return Mat3f();
}


String RayTracer::computeMD5( const std::vector<Vec3f>& vertices )
{
    unsigned char digest[16];
    MD5Buffer( (void*)&vertices[0], sizeof(Vec3f)*vertices.size(), (unsigned int*)digest );

    // turn into string
    char ad[33];
    for ( int i = 0; i < 16; ++i )
        ::sprintf( ad+i*2, "%02x", digest[i] );
    ad[32] = 0;

    return FW::String( ad );
}


// --------------------------------------------------------------------------


RayTracer::RayTracer()
{
}

RayTracer::~RayTracer()
{
}


void RayTracer::loadHierarchy(const char* filename, std::vector<RTTriangle>& triangles)
{
    // YOUR CODE HERE (R2):
    m_triangles = &triangles;
}

void RayTracer::saveHierarchy(const char* filename, const std::vector<RTTriangle>& triangles) {
    // YOUR CODE HERE (R2)
}

void partitionPrimitives(std::vector<RTTriangle>& triangles, Plane split, std::vector<RTTriangle>& left,
	std::vector<RTTriangle>& right) {

}

Plane chooseSplit(std::vector<RTTriangle>& triangles) {
	return Plane();
}

void constructTree(std::vector<RTTriangle>& triangles, Node rNode) {
	AABB rAABB = AABB();
	rNode.box = rAABB;
	
	// The AABB of the first triangle
	rAABB.max = triangles[0].max;
	rAABB.min = triangles[0].min;

	// The biggest Bound considered all the triangles
	for (size_t i = 1; i < triangles.size; i++)
	{
		rAABB.max = FW::max(rAABB.max, triangles[i].max);
		rAABB.min = FW::min(rAABB.min, triangles[i].min);
	}	

	Plane split = chooseSplit(triangles);

	std::vector<RTTriangle>& leftChildList;
	std::vector<RTTriangle>& rightChildList;
	partitionPrimitives(triangles, split, leftChildList, rightChildList);
	rNode.leftChild = NULL;
	constructTree(leftChildList, rNode.leftChild);
	rNode.rightChild = NULL;
	constructTree(rightChildList, rNode.rightChild);

	rNode.startPrim = 1;
	rNode.endPrim = triangles.size;
}

void RayTracer::constructHierarchy(std::vector<RTTriangle>& triangles, SplitMode splitMode) {
    // YOUR CODE HERE (R1):
	m_triangles = &triangles;
	
	Node rNode = Node();
	constructTree(triangles, rNode);
	
	// split.dot(triangles[i].centroid) >= 0	
	//p = std::partition(triangles.begin, triangles.back, std::ptr_fun(is_half));
}




RaycastResult RayTracer::raycast(const Vec3f& orig, const Vec3f& dir) const {
	++m_rayCount;

    // YOUR CODE HERE (R1):
    // This is where you hierarchically traverse the tree you built!
    // You can use the existing code for the leaf nodes.

    float tmin = 1.0f, umin = 0.0f, vmin = 0.0f;
    int imin = -1;

    RaycastResult castresult;

    // Naive loop over all triangles.
    for ( size_t i = 0; i < m_triangles->size(); ++i )
    {
        float t, u, v;
        if ( (*m_triangles)[i].intersect_woop( orig, dir, t, u, v ) )
        {
            if ( t > 0.0f && t < tmin )
            {
                imin = i;
                tmin = t;
                umin = u;
                vmin = v;
            }
        }
    }

    if (imin != -1) {
        castresult = RaycastResult(&(*m_triangles)[imin], tmin, umin, vmin, orig + tmin*dir, orig, dir);
    }
    return castresult;
}


} // namespace FW